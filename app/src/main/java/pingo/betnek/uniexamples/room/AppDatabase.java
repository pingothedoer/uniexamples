package pingo.betnek.uniexamples.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import pingo.betnek.uniexamples.room.simple.SimpleContact;
import pingo.betnek.uniexamples.room.simple.SimpleContactDao;

@Database(entities = {SimpleContact.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract SimpleContactDao simpleContactDao();

}
