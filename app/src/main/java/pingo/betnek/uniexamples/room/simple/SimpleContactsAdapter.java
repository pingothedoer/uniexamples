package pingo.betnek.uniexamples.room.simple;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pingo.betnek.uniexamples.R;
import pingo.betnek.uniexamples.room.ContactsHolder;

public class SimpleContactsAdapter extends RecyclerView.Adapter<ContactsHolder> {

    private final ArrayList<SimpleContact> contactsList;

    public SimpleContactsAdapter(ArrayList<SimpleContact> simpleContactList) {
        this.contactsList = simpleContactList;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ContactsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsHolder holder, int position) {
        final SimpleContact simpleContact = contactsList.get(holder.getAdapterPosition());
        holder.tvName.setText(simpleContact.getName());
        holder.tvEmail.setText(simpleContact.getEmail());
        holder.tvMobile.setText(simpleContact.getMobile());

    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }
}
