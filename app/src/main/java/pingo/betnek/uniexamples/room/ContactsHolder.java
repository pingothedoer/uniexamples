package pingo.betnek.uniexamples.room;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import pingo.betnek.uniexamples.R;

public class ContactsHolder extends RecyclerView.ViewHolder {

    public final TextView tvName;
    public final TextView tvEmail;
    public final TextView tvMobile;

    public ContactsHolder(View itemView) {
        super(itemView);

        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        tvEmail = (TextView) itemView.findViewById(R.id.tv_email);
        tvMobile = (TextView) itemView.findViewById(R.id.tv_mobile);
    }


}
