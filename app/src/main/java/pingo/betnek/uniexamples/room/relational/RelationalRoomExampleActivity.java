package pingo.betnek.uniexamples.room.relational;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pingo.betnek.uniexamples.R;
import pingo.betnek.uniexamples.network.HttpHandler;
import pingo.betnek.uniexamples.room.simple.SimpleContact;
import pingo.betnek.uniexamples.room.simple.SimpleContactsAdapter;

public class RelationalRoomExampleActivity extends AppCompatActivity {


    // URL to get contacts JSON
    String url = "https://api.androidhive.info/contacts/";
    ArrayList<SimpleContact> simpleContactList = new ArrayList<>();
    RecyclerView listView;
    Button btnGetData;
    private SimpleContactsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_example);

        btnGetData = (Button) findViewById(R.id.btn_get_data);
        listView = (RecyclerView) findViewById(R.id.list_view);
        listView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SimpleContactsAdapter(simpleContactList);
        listView.setAdapter(adapter);


        btnGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetContacts(url).execute();
            }
        });

    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        private final String url;
        private String TAG = RelationalRoomExampleActivity.class.getSimpleName();
        private ProgressDialog pDialog;

        private GetContacts(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(RelationalRoomExampleActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contactsJsonArray = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                    for (int index = 0; index < contactsJsonArray.length(); index++) {

                        JSONObject contactJsonObject = contactsJsonArray.getJSONObject(index);

                        String id = contactJsonObject.getString("id");
                        String name = contactJsonObject.getString("name");
                        String email = contactJsonObject.getString("email");
                        String address = contactJsonObject.getString("address");
                        String gender = contactJsonObject.getString("gender");

                        // Phone node is JSON Object
                        JSONObject phoneJsonObject = contactJsonObject.getJSONObject("phone");
                        String mobile = phoneJsonObject.getString("mobile");
                        String home = phoneJsonObject.getString("home");
                        String office = phoneJsonObject.getString("office");


                        SimpleContact simpleContact = new SimpleContact();

                        simpleContact.setId(id);
                        simpleContact.setName(name);
                        simpleContact.setEmail(email);
                        simpleContact.setAddress(address);
                        simpleContact.setGender(gender);

                        // adding simpleContact to simpleContact list
                        simpleContactList.add(simpleContact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            adapter.notifyDataSetChanged();

        }

    }

}
