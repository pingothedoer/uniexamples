package pingo.betnek.uniexamples.room.simple;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;


@Dao
public interface SimpleContactDao {

    @Query("SELECT * FROM simple_contact")
    List<SimpleContact> getAll();

    @Query("SELECT * FROM simple_contact WHERE contact_gender = :gender")
    List<SimpleContact> getAllByGender(String gender);

    @Insert
    void insertAll(SimpleContact... contacts);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SimpleContact contacts);

    @Delete
    void delete(SimpleContact user);

    @Query("DELETE FROM simple_contact WHERE contact_name = :name")
    void deleteByName(String name);
}
