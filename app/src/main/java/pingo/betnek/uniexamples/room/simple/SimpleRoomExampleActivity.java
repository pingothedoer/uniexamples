package pingo.betnek.uniexamples.room.simple;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pingo.betnek.uniexamples.MyApp;
import pingo.betnek.uniexamples.R;
import pingo.betnek.uniexamples.network.HttpHandler;

public class SimpleRoomExampleActivity extends AppCompatActivity {

    // URL to get contacts JSON
    String url = "https://api.androidhive.info/contacts/";
    ArrayList<SimpleContact> simpleContactList = new ArrayList<>();
    RecyclerView listView;
    Button btnGetData;
    private SimpleContactsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_example);

        btnGetData = (Button) findViewById(R.id.btn_get_data);
        listView = (RecyclerView) findViewById(R.id.list_view);
        listView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SimpleContactsAdapter(simpleContactList);
        listView.setAdapter(adapter);


        btnGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetContacts(url).execute();
            }
        });

    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        private final String url;
        private String TAG = SimpleRoomExampleActivity.class.getSimpleName();
        private ProgressDialog pDialog;

        private GetContacts(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(SimpleRoomExampleActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Make an array list
                    ArrayList<SimpleContact> contactArrayList = new ArrayList<>();

                    // Getting JSON Array node
                    JSONArray contactsJsonArray = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                    for (int index = 0; index < contactsJsonArray.length(); index++) {

                        JSONObject contactJsonObject = contactsJsonArray.getJSONObject(index);

                        String id = contactJsonObject.getString("id");
                        String name = contactJsonObject.getString("name");
                        String email = contactJsonObject.getString("email");
                        String address = contactJsonObject.getString("address");
                        String gender = contactJsonObject.getString("gender");

                        // Phone node is JSON Object
                        JSONObject phoneJsonObject = contactJsonObject.getJSONObject("phone");
                        String mobile = phoneJsonObject.getString("mobile");


                        SimpleContact simpleContact = new SimpleContact();

                        simpleContact.setId(id);
                        simpleContact.setName(name);
                        simpleContact.setEmail(email);
                        simpleContact.setAddress(address);
                        simpleContact.setGender(gender);
                        simpleContact.setMobile(mobile);

                        // adding simpleContact to simpleContact list
                        contactArrayList.add(simpleContact);

                    }


                    /**
                     * Storing Data in Room Database
                     */
                    for (SimpleContact contact : contactArrayList) {
                        MyApp.getApp().getDB().simpleContactDao().insert(contact);
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            Toast.makeText(SimpleRoomExampleActivity.this, "Data Saved", Toast.LENGTH_SHORT).show();


            // Get data from database and send it to adapter
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    // Get Data
                    List<SimpleContact> contacts = MyApp.getApp().getDB().simpleContactDao().getAll();
                    simpleContactList.addAll(contacts);

                    // Now update UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            });

        }

    }

}
