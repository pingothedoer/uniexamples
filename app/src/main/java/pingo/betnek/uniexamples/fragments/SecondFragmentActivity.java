package pingo.betnek.uniexamples.fragments;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import pingo.betnek.uniexamples.R;

public class SecondFragmentActivity extends AppCompatActivity {

    FrameLayout fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_fragment);
        fragmentContainer = findViewById(R.id.fragment_container);

        // Create a new Fragment to be placed in the activity layout
        NameFragment nameFragment = new NameFragment();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment_container, nameFragment);
        transaction.commit();


        findViewById(R.id.nextBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AgeFragment ageFragment = new AgeFragment();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, ageFragment)
                        .addToBackStack(null)
                        .commit();

            }
        });

    }
}
