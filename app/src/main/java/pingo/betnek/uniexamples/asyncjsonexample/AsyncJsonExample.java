package pingo.betnek.uniexamples.asyncjsonexample;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pingo.betnek.uniexamples.R;
import pingo.betnek.uniexamples.network.HttpHandler;

/**
 * Following is the sample JSON that we are going to parse in this tutorial.
 * This is very simple JSON which gives us list of contacts where each node contains
 * contact information like name, email, address, gender and phone numbers.
 * <p>
 * You can get this JSON data by accessing https://api.androidhive.info/contacts/
 * <p>
 * {
 * "contacts": [
 * {
 * "id": "c200",
 * "name": "Ravi Tamada",
 * "email": "ravi@gmail.com",
 * "address": "xx-xx-xxxx,x - street, x - country",
 * "gender" : "male",
 * "phone": {
 * "mobile": "+91 0000000000",
 * "home": "00 000000",
 * "office": "00 000000"
 * }
 * },
 * {
 * "id": "c201",
 * "name": "Johnny Depp",
 * "email": "johnny_depp@gmail.com",
 * "address": "xx-xx-xxxx,x - street, x - country",
 * "gender" : "male",
 * "phone": {
 * "mobile": "+91 0000000000",
 * "home": "00 000000",
 * "office": "00 000000"
 * }
 * },
 * .
 * .
 * .
 * ]
 * }
 */

public class AsyncJsonExample extends AppCompatActivity {


    // URL to get contacts JSON
    String url = "https://api.androidhive.info/contacts/";
    ArrayList<Contact> contactList = new ArrayList<>();
    RecyclerView listView;
    Button btnGetData;
    ContactsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_json_example);

        btnGetData = (Button) findViewById(R.id.btn_get_data);

        listView = (RecyclerView) findViewById(R.id.list_view);

        listView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ContactsAdapter(contactList);


        listView.setAdapter(adapter);


        btnGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetContacts(url).execute();
            }
        });

    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        private final String url;
        private String TAG = AsyncJsonExample.class.getSimpleName();
        private ProgressDialog pDialog;

        private GetContacts(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(AsyncJsonExample.this);
            pDialog.setTitle("Getting Contacts");
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (!TextUtils.isEmpty(jsonStr)) {

                try {

                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {

                        JSONObject c = contacts.getJSONObject(i);

                        String id = c.getString("id");
                        String name = c.getString("name");
                        String email = c.getString("email");
                        String address = c.getString("address");
                        String gender = c.getString("gender");

                        // Phone node is JSON Object
                        JSONObject phone = c.getJSONObject("phone");
                        String mobile = phone.getString("mobile");
                        String home = phone.getString("home");
                        String office = phone.getString("office");

                        Phone phObject = new Phone();
                        phObject.setHome(home);
                        phObject.setMobile(mobile);
                        phObject.setOffice(office);


                        Contact contact = new Contact();

                        contact.setId(id);
                        contact.setName(name);
                        contact.setEmail(email);
                        contact.setAddress(address);
                        contact.setGender(gender);
                        contact.setPhone(phObject);

                        // adding contact to contact list
                        contactList.add(contact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            adapter.notifyDataSetChanged();

        }

    }

}
