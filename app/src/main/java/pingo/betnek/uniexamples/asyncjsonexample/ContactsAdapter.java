package pingo.betnek.uniexamples.asyncjsonexample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pingo.betnek.uniexamples.R;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsHolder> {


    private final ArrayList<Contact> contactsList;

    public ContactsAdapter(ArrayList<Contact> contactList) {
        this.contactsList = contactList;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ContactsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsHolder holder, int position) {
        final Contact contact = contactsList.get(holder.getAdapterPosition());
        holder.tvName.setText(contact.getName());
        holder.tvEmail.setText(contact.getEmail());
        holder.tvMobile.setText(contact.getPhone().getMobile());

    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

}
