package pingo.betnek.uniexamples;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import pingo.betnek.uniexamples.asyncjsonexample.AsyncJsonExample;
import pingo.betnek.uniexamples.fragments.BaseExampleActivity;
import pingo.betnek.uniexamples.room.simple.SimpleRoomExampleActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAsyncJson;
    private Button btnRoom;
    private Button btnFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init views
        btnAsyncJson = (Button) findViewById(R.id.btn_async_json);
        btnRoom = (Button) findViewById(R.id.btn_room);
        btnFragment = (Button) findViewById(R.id.btn_fragment);

        btnAsyncJson.setOnClickListener(this);
        btnRoom.setOnClickListener(this);
        btnFragment.setOnClickListener(this);

    }


    @Override
    public void onClick(View clickedView) {

        switch (clickedView.getId()) {

            case R.id.btn_async_json:
                // AsyncTask with Json parsing example
                startActivity(new Intent(MainActivity.this, AsyncJsonExample.class));
                break;

            case R.id.btn_room:
                // Room with JSON parsing example
                startActivity(new Intent(MainActivity.this, SimpleRoomExampleActivity.class));
                break;


            case R.id.btn_fragment:
                startActivity(new Intent(MainActivity.this, BaseExampleActivity.class));
                break;


        }
    }
}
