package pingo.betnek.uniexamples;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import pingo.betnek.uniexamples.room.AppDatabase;

/**
 * Created by subhan
 * on 27/10/2017.
 */

public class MyApp extends Application {

    private static MyApp mInstance;
    private static final String DATABASE_NAME = "UniDatabase";
    private AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();

        // create database
        database = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                DATABASE_NAME)
                .build();

        mInstance = this;
    }


    public AppDatabase getDB() {
        return database;
    }

    public static MyApp getApp() {
        return mInstance;
    }

    public static Context getContext() {
        if (mInstance == null) {
            mInstance = getApp();
            return mInstance.getApplicationContext();
        } else {
            return mInstance.getApplicationContext();
        }
    }

}
